package com.academiamoviles.lab02mascota

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //1.- Evento onClick
        btnEnviar.setOnClickListener {
            //2.- Obtener la informacion de las cajas de texto
            val nombres = tvNombreResultado.text.toString()
            val edad = tvEdadResultado.text.toString()
            //3.- Validar que los textos no esten vacios
            if (nombres.isEmpty()) {
                Toast.makeText(this, "Debe ingresar sus nombres", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edad.isEmpty()) {
                Toast.makeText(this, "Debe ingresar su edad", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //4.- Obtener la mascota
            var mascota = ""
            if (rbPerro.isChecked) {
                mascota = "icon_perro.png"
            } else if (rbGato.isChecked) {
                mascota = "icon_gato.png"
            } else {
                mascota = "icon_conejo.png"
            }
            //5.- Obtener las vacunas
            var vacuna = ""
            if (chkRabia.isChecked) {
                vacuna = vacuna + "Rabia" + " "
            }
            if (chkDistemper.isChecked) {
                vacuna = vacuna + "Distemper" + " "
            }
            if (chkPolio.isChecked) {
                vacuna = vacuna + "Polio" + " "
            }
            //6.- Guardar informacion en el Bundle
            val bundle = Bundle()
            bundle.putString("KEY_NOMBRES", nombres)
            bundle.putString("KEY_EDAD", edad)
            bundle.putString("KEY_MASCOTA", mascota)
            bundle.putString("KEY_VACUNA", vacuna)
            //7.- Pasar a la otra pantalla
            val intent = Intent(this,DestinoActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }

    }
}