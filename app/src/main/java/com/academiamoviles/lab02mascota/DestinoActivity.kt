package com.academiamoviles.lab02mascota

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_destino.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)
        //Vamos a recibir el bundle a traves del intent
        val bundleRecepcion:Bundle? = intent.extras
        //Que pasa si el bundle nos pasa vacio
        val nombre = bundleRecepcion?.getString("KEY_NOMBRES","")
        val edad = bundleRecepcion?.getString("KEY_EDAD","")
        val mascota = bundleRecepcion?.getString("KEY_MASCOTA","")
        val vacuna = bundleRecepcion?.getString("KEY_VACUNA","")
        tvNombreResultado.text=nombre
        tvEdadResultado.text=edad
        if (mascota=="icon_perro.png"){
            imgFoto.setImageResource(R.drawable.icon_perro)
        }else if(mascota=="icon_gato.png"){
            imgFoto.setImageResource(R.drawable.icon_gato)
        }else {
            imgFoto.setImageResource(R.drawable.icon_conejo)
        }
        tvVacunaResultado.text=vacuna
    }
}